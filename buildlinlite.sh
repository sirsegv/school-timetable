#!/bin/bash

if [[ $1 = '-a' ]]
then
ARCH=x86_64 appimagetool ~/Temporary/dist_lite/timetable ~/Temporary/dist_lite/timetable.app
else
mkdir ~/Temporary/dist_lite
mkdir ~/Temporary/dist_lite/timetable
mkdir ~/Temporary/dist_lite/timetable/lib
mkdir ~/Temporary/dist_lite/timetable/assets
mkdir ~/Temporary/dist_lite/timetable/assets/ui
cp -r ./timetable.py ~/Temporary/dist_lite/timetable/timetable.py
cp -r ./assets/ui/ ~/Temporary/dist_lite/timetable/assets/
cp -r ./lib/ ~/Temporary/dist_lite/timetable/

cp -r ./appimage/AppRun_lite ~/Temporary/dist_lite/timetable/AppRun
cp -r ./appimage/Timetable.desktop ~/Temporary/dist_lite/timetable/Timetable.desktop
cp -r ./appimage/Timetable.desktop ~/Temporary/dist_lite/timetable/assets/Timetable.desktop
cp -r ./assets/ui/icon.png ~/Temporary/dist_lite/timetable/
ARCH=x86_64 appimagetool ~/Temporary/dist_lite/timetable ~/Temporary/dist_lite/timetable-x86_64.app
fi
