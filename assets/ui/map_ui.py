# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'map.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractScrollArea, QApplication, QFrame, QGraphicsView,
    QHBoxLayout, QLabel, QLineEdit, QPushButton,
    QSizePolicy, QSpacerItem, QVBoxLayout, QWidget)
import backgrounds_rc

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.setWindowModality(Qt.ApplicationModal)
        Dialog.resize(987, 710)
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.mapFrame = QFrame(Dialog)
        self.mapFrame.setObjectName(u"mapFrame")
        self.mapFrame.setStyleSheet(u"#mapFrame {\n"
"border-radius: 10px;\n"
"}")
        self.mapFrame.setFrameShape(QFrame.StyledPanel)
        self.mapFrame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.mapFrame)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.header = QHBoxLayout()
        self.header.setObjectName(u"header")
        self.labelTitle = QLabel(self.mapFrame)
        self.labelTitle.setObjectName(u"labelTitle")
        self.labelTitle.setStyleSheet(u"font: 75 18pt;")

        self.header.addWidget(self.labelTitle)

        self.editLocation = QLineEdit(self.mapFrame)
        self.editLocation.setObjectName(u"editLocation")

        self.header.addWidget(self.editLocation)

        self.buttonFind = QPushButton(self.mapFrame)
        self.buttonFind.setObjectName(u"buttonFind")

        self.header.addWidget(self.buttonFind)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.header.addItem(self.horizontalSpacer)

        self.buttonReset = QPushButton(self.mapFrame)
        self.buttonReset.setObjectName(u"buttonReset")

        self.header.addWidget(self.buttonReset)

        self.buttonClose = QPushButton(self.mapFrame)
        self.buttonClose.setObjectName(u"buttonClose")

        self.header.addWidget(self.buttonClose)


        self.verticalLayout_2.addLayout(self.header)

        self.map = QGraphicsView(self.mapFrame)
        self.map.setObjectName(u"map")
        self.map.setMinimumSize(QSize(0, 0))
        self.map.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.map.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.map.setSizeAdjustPolicy(QAbstractScrollArea.AdjustIgnored)
        self.map.setRenderHints(QPainter.Antialiasing|QPainter.SmoothPixmapTransform|QPainter.TextAntialiasing)
        self.map.setDragMode(QGraphicsView.ScrollHandDrag)
        self.map.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)

        self.verticalLayout_2.addWidget(self.map)


        self.verticalLayout.addWidget(self.mapFrame)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Dialog", None))
        self.labelTitle.setText(QCoreApplication.translate("Dialog", u"Map", None))
        self.editLocation.setPlaceholderText(QCoreApplication.translate("Dialog", u"Room Number", None))
        self.buttonFind.setText(QCoreApplication.translate("Dialog", u"Find", None))
        self.buttonReset.setText(QCoreApplication.translate("Dialog", u"Reset", None))
        self.buttonClose.setText("")
    # retranslateUi

