from collections import OrderedDict
foreground = OrderedDict({
	"Default": "rgb(255, 255, 255)",
	"Light": "rgb(35, 38, 39)",
	"Woodcroft": "rgb(158, 148, 139)",
	"Hot Dog Stand": "rgb(255, 255, 0)",
	"Galaxy": "rgb(200, 90, 255)",
	"Meenu": "rgb(255, 60, 90)",
	"Keely": "rgb(255, 127, 39)",
	"Commodore": "rgb(163, 150, 255)",
	"Terminal": "rgb(0, 220, 0)",
	"Ubuntu": "rgb(255, 255, 255)",
	"Blue Flame": "rgb(60, 80, 255)"
})
background = {
	"Default": "rgb(34, 34, 34)",
	"Light": "rgb(252, 252, 252)",
	"Woodcroft": "rgb(33, 64, 105)",
	"Hot Dog Stand": "rgb(255, 0, 0)",
	"Galaxy": "rgb(0, 0, 0)",
	"Meenu": "rgb(20, 20, 20)",
	"Keely": "rgb(120, 180, 200)",
	"Commodore": "rgb(78, 68, 216)",
	"Terminal": "rgb(0, 0, 0)",
	"Ubuntu": "rgb(248, 97, 9)",
	"Blue Flame": "rgb(0, 0, 0)"
}
backgroundalt = {
	"Default": "rgb(68, 68, 68)",
	"Light": "rgb(230, 230, 230)",
	"Woodcroft": "rgb(30, 45, 70)",
	"Hot Dog Stand": "rgb(255, 100, 0)",
	"Galaxy": "rgb(35, 5, 50)",
	"Meenu": "rgb(50, 0, 0)",
	"Keely": "rgb(70, 190, 215)",
	"Commodore": "rgb(90, 90, 190)",
	"Terminal": "rgb(0, 80, 0)",
	"Ubuntu": "rgb(250, 177, 21)",
	"Blue Flame": "rgb(0, 15, 100)"
}
pictures = {
	"Default": "view",
	"Light": "view",
	"Woodcroft": "woodcroft",
	"Hot Dog Stand": "hotdog",
	"Galaxy": "galaxy",
	"Meenu": "meenu",
	"Keely": "keely",
	"Commodore": "commodore",
	"Terminal": "terminal",
	"Ubuntu": "ubuntu",
	"Blue Flame": "blueflame"
}
backpics = {
	"Default": "None",
	"Light": "None",
	"Woodcroft": "None",
	"Hot Dog Stand": "None",
	"Galaxy": "None",
	"Meenu": "None",
	"Keely": "None",
	"Commodore": "None",
	"Terminal": "None",
	"Ubuntu": "None",
	"Blue Flame": ":/backgrounds/blueflameback.png"
}
light = {
	"Default": False,
	"Light": True,
	"Woodcroft": False,
	"Hot Dog Stand": True,
	"Galaxy": False,
	"Meenu": False,
	"Keely": True,
	"Commodore": True,
	"Terminal": False,
	"Ubuntu": True,
	"Blue Flame": False
}