import datetime

weekno = int(datetime.date.today().isocalendar()[1])
monthno = int(datetime.date.today().strftime("%m"))
day = int(datetime.date.today().day)
dayno = int(datetime.date.today().weekday())
scweek = ""

if datetime.date.today().strftime("%Y")=="2022":
	# 3 feb to 14 april
	if (monthno == 2 and day >= 3) or (monthno >= 2 and monthno <= 4) or (monthno == 4 and day <= 14):
		term = "Term 1"
		if weekno == 5 and dayno >= 3 and dayno < 4:
			scweek = "Week 1"
		elif weekno >= 6 and weekno <= 14 and dayno >= 0 and dayno <= 4:
			scweek = "Week " + str(weekno - 4)
		elif weekno == 15 and dayno >= 0 and dayno <= 3:
			scweek = "Week 11"
		elif dayno == 5 or dayno == 6:
			scweek = "Weekend!"
		else:
			scweek = "null"
	# 3 may to 30 june
	elif (monthno == 5 and day >= 3) or (monthno == 6):
		term = "Term 2"
		if weekno == 18 and dayno >= 1 and dayno <= 4:
			scweek = "Week 1"
		elif weekno >= 19 and weekno <= 25 and dayno >= 0 and dayno <= 4:
			scweek = "Week " + str(weekno - 17)
		elif weekno == 26 and dayno >= 0 and dayno <= 3:
			scweek = "Week 9"
		elif dayno == 5 or dayno == 6:
			scweek = "Weekend!"
		else:
			scweek = "null"
	# 26 july to 29 september
	elif (monthno == 7 and day >= 26) or (monthno == 8) or (monthno == 9 and day <= 29):
		term = "Term 3"
		if weekno == 30 and dayno >= 1 and dayno <= 4:
			scweek = "Week 1"
		elif weekno >= 31 and weekno <= 38 and dayno >= 0 and dayno <= 4:
			scweek = "Week " + str(weekno - 29)
		elif weekno == 39 and dayno >= 0 and dayno <= 3:
			scweek = "Week 10"
		elif dayno == 5 or dayno == 6:
			scweek = "Weekend!"
		else:
			scweek = "null"
	# 18 october to 15 december
	elif (monthno == 10 and day >= 18) or (monthno == 11) or (monthno == 12 and day <= 15):
		term = "Term 4"
		if weekno == 42 and dayno >= 1 and dayno <= 4:
			scweek = "Week 1"
		elif weekno >= 43 and weekno <= 49 and dayno >= 0 and dayno <= 4:
			scweek = "Week " + str(weekno - 41)
		elif weekno == 50 and dayno >= 0 and dayno <= 3:
			scweek = "Last week!"
		elif dayno == 5 or dayno == 6:
			scweek = "Weekend!"
		else:
			scweek = "null"
	else:
		term = 0
